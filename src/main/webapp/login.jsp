<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="stylesheet" type="text/css" href="/blog/css/style.css" />
  </head>
  <body>
    <div class="content">
      <form class="form_login" method="post">
        <h1>${message}</h1>
        <input type="text" name="login" placeholder="login" /><br />
        <input type="password" name="password" placeholder="password" /><br />
        <button type="submit" name="action" value="login">Sign in</button>
        <button type="submit" name="action" value="register">Sign up</button>
        <h1><a href="https://bitbucket.org/rundot/blog_2/src">sources: Bitbucket repo</a></h1>
      </form>
    </div>
  </body>
</html>