<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="stylesheet" type="text/css" href="/blog/css/style.css" />
  </head>
  <body>
    <div class="menu">
      <div id="back"><a href="/blog/post"><img src="/blog/img/back.png" /></a></div>
      <form  method="post" action="/blog/login"><h1>Welcome, ${user.fullname} <button type="submit" name="action" value="logout">Sign out</button></h1></form>
    </div>
    <div class="content">
      <div class="post">
        <h1>${post.topic}</h1>
        ${post.content}
        <h3>Posted by ${post.user.fullname} @ <fmt:formatDate type="both" value="${post.timestamp}" /></h3>
      </div>
      <h1>Comments</h1>
      <c:forEach var="comment" items="${commentList}">
        <div class="comment">
          ${comment.content}<br />
          <h3>Commented by ${comment.user.fullname} @ <fmt:formatDate type="both" value="${comment.timestamp}" /></h3>
        </div>
      </c:forEach>
      <h1>Leave your comment</h1>
      <form class="form_comment" method="post" action="/blog/comment">
        <input type="hidden" name="id" value="${post.id}" />
        <textarea name="content" placeholder="Your comment"></textarea><br />
        <button type="submit" name="submit">Post</button>
      </form>
    </div>
  </body>
</html>