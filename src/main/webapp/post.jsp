<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="stylesheet" type="text/css" href="/blog/css/style.css" />
  </head>
  <body>
    <div class="menu">
      <form  method="post" action="/blog/login"><h1>Welcome, ${user.fullname} <button type="submit" name="action" value="logout">Sign out</button></h1></form>
    </div>
    <div class="content">
      <form class="form_post" method="post">
        <input type="text" name="topic" placeholder = "topic" /><br />
        <textarea name="content" placeholder="Your post"></textarea><br />
        <button type="submit" name="submit">Post</button>
      </form>
      <c:forEach var="post" items="${postList}">
        <div class="post">
          <h2><a href="/blog/post/${post.id}"><c:out value="${post.topic}" default="[без темы]" /></a></h1>
          <h3>Posted by ${post.user.fullname} @ <fmt:formatDate type="both" value="${post.timestamp}" /></h3>
          <h3>Comments ${post.commentList.size()}</h3>
        </div>
      </c:forEach>
    </div>
  </body>
</html>