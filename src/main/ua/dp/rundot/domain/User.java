package ua.dp.rundot.domain;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class User {
    private int id;
    private String login;
    private String password;
    private String fullname;

    public User(int id, String login, String password, String fullname) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.fullname = fullname;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getFullname() {
        return fullname;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", fullname='" + fullname + '\'' +
                '}';
    }
}