package ua.dp.rundot.domain;

import java.sql.Timestamp;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class Post {
    protected int id;
    protected Timestamp timestamp;
    protected String topic;
    protected String content;
    protected int userId;

    public Post(int id, Timestamp timestamp, String topic, String content, int userId) {
        this.id = id;
        this.timestamp = timestamp;
        this.topic = topic;
        this.content = content;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public String getTopic() {
        return topic;
    }

    public String getContent() {
        return content;
    }

    public int getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", topic='" + topic + '\'' +
                ", content='" + content + '\'' +
                ", userId=" + userId +
                '}';
    }
}