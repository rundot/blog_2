package ua.dp.rundot.domain;

import java.sql.Timestamp;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class Comment {
    protected int id;
    protected Timestamp timestamp;
    protected String content;
    protected int userId;
    protected int postId;

    public Comment(int id, Timestamp timestamp, String content, int userId, int postId) {
        this.id = id;
        this.timestamp = timestamp;
        this.content = content;
        this.userId = userId;
        this.postId = postId;
    }

    public int getId() {
        return id;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public String getContent() {
        return content;
    }

    public int getUserId() {
        return userId;
    }

    public int getPostId() {
        return postId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", content='" + content + '\'' +
                ", userId=" + userId +
                ", postId=" + postId +
                '}';
    }
}