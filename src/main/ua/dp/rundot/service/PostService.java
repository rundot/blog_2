package ua.dp.rundot.service;

import ua.dp.rundot.dao.PostDao;
import ua.dp.rundot.dao.postgreSQL.PostDaoImpl;
import ua.dp.rundot.domain.Post;
import ua.dp.rundot.dts.PostDts;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class PostService {

    private static PostDao postDao = new PostDaoImpl();

    public static int save(Post post) {
        return postDao.save(post);
    }

    public static Post get(int id) {
        return postDao.get(id);
    }

    public static PostDts extendedGet(int id) {
        PostDts postDts = new PostDts(get(id));
        postDts.setUser(UserService.get(postDts.getUserId()));
        return postDts;
    }

    public static List<Post> list() {
        return postDao.list();
    }

    public static List<PostDts> extendedList() {
        List<PostDts> postDtsList = list().stream().map(post -> new PostDts(post)).collect(Collectors.toList());
        postDtsList.stream().forEach(
                postDts -> {postDts.setCommentList(CommentService.listByPostId(postDts.getId()));
                            postDts.setUser(UserService.get(postDts.getUserId()));
                });
        return postDtsList;
    }

    public static void main(String[] args) {
        System.out.println(extendedList());
    }

}