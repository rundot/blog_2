package ua.dp.rundot.service;

import ua.dp.rundot.dao.UserDao;
import ua.dp.rundot.dao.postgreSQL.UserDaoImpl;
import ua.dp.rundot.domain.User;

import java.util.List;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class UserService {

    private static UserDao userDao = new UserDaoImpl();

    public static int save(User user) {
        return userDao.save(user);
    }

    public static User get(int id) {
        return userDao.get(id);
    }

    public static List<User> list() {
        return userDao.list();
    }

    public static int getIdByLoginPassword(String login, String password) {
        User user = list().stream().filter(u -> (u.getLogin().equals(login) && u.getPassword().equals(password))).findFirst().orElse(null);
        if (user == null) return 0;
        else return user.getId();
    }

}