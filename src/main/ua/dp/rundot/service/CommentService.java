package ua.dp.rundot.service;

import ua.dp.rundot.dao.CommentDao;
import ua.dp.rundot.dao.postgreSQL.CommentDaoImpl;
import ua.dp.rundot.domain.Comment;
import ua.dp.rundot.dts.CommentDts;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class CommentService {

    private static CommentDao commentDao = new CommentDaoImpl();

    public static int save(Comment comment) {
        return commentDao.save(comment);
    }

    public static Comment get(int id) {
        return commentDao.get(id);
    }

    public static List<Comment> list() {
        return commentDao.list();
    }

    public static List<Comment> listByPostId(int postId) {
        return list().stream().filter(comment -> comment.getPostId() == postId).collect(Collectors.toList());
    }

    public static List<CommentDts> extendedListByPostId(int postId) {
        List<CommentDts> commentDtsList = listByPostId(postId)
                .stream()
                .map(comment -> new CommentDts(comment))
                .collect(Collectors.toList());
        commentDtsList
                .stream()
                .forEach(commentDts -> commentDts.setUser(UserService.get(commentDts.getUserId())));
        return commentDtsList;
    }

}