package ua.dp.rundot.controller;

import ua.dp.rundot.domain.Comment;
import ua.dp.rundot.service.CommentService;
import ua.dp.rundot.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class CommentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        int postId = Integer.parseInt(req.getParameter("id"));
        int userId = (int)req.getSession().getAttribute("userId");
        String content = req.getParameter("content");
        Comment comment = new Comment(0, null, content, userId, postId);
        CommentService.save(comment);
        resp.sendRedirect("/blog/post/" + postId);
    }
}