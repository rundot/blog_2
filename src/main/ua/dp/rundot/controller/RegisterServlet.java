package ua.dp.rundot.controller;

import ua.dp.rundot.domain.User;
import ua.dp.rundot.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Created by rundot on 17.01.2017.
 */
public class RegisterServlet extends HttpServlet {

    Pattern registerPattern = Pattern.compile("/blog/register");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestURI = req.getRequestURI();
        if (registerPattern.matcher(requestURI).matches()) {
            req.getRequestDispatcher("/register.jsp").forward(req, resp);
            return;
        }
        resp.sendError(404);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String fullname = req.getParameter("fullname");
        User user = new User(0, login, password, fullname);
        if (UserService.save(user) > 0) {
            resp.sendRedirect("/blog/login?result=ok");
        } else {
            resp.sendRedirect("/blog/register");
        }

    }
}
