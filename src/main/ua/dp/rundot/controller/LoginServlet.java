package ua.dp.rundot.controller;

import ua.dp.rundot.service.UserService;
import ua.dp.rundot.util.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class LoginServlet extends HttpServlet {

    Pattern loginPattern = Pattern.compile("/blog/(login)*");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestURI = req.getRequestURI();
        if (loginPattern.matcher(requestURI).matches()) {
            String result = req.getParameter("result");
            if (result != null) {
                String message = "";
                switch (result) {
                    case "error":
                        message = "Incorrect login/password";
                        break;
                    case "signout":
                        message = "You have signed out";
                        break;
                    case "ok":
                        message = "User created.<br />You can sign in now.";
                        break;
                }
                req.setAttribute("message", message);
            }
            req.getRequestDispatcher("login.jsp").forward(req, resp);
            return;
        }
        resp.sendError(404);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        String action = req.getParameter("action");

        if ("register".equals(action)) {
            resp.sendRedirect("/blog/register");
            return;
        }

        if ("login".equals(action)) {
            String login = req.getParameter("login");
            String password = req.getParameter("password");
            int userId = UserService.getIdByLoginPassword(login, password);
            if (userId > 0) {
                req.getSession().setAttribute("userId", userId);
                resp.sendRedirect("/blog/post");
            } else {
                resp.sendRedirect("/blog/login?result=error");
            }
        }

        if ("logout".equals(action)) {
            req.getSession().removeAttribute("userId");
            resp.sendRedirect("/blog/login?result=signout");
        }

    }
}