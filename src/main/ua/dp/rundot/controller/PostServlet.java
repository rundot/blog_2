package ua.dp.rundot.controller;

import ua.dp.rundot.domain.Comment;
import ua.dp.rundot.domain.Post;
import ua.dp.rundot.dts.CommentDts;
import ua.dp.rundot.dts.PostDts;
import ua.dp.rundot.service.CommentService;
import ua.dp.rundot.service.PostService;
import ua.dp.rundot.service.UserService;
import ua.dp.rundot.util.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class PostServlet extends HttpServlet {

    private Pattern postListPattern = Pattern.compile("/blog/post");
    private Pattern postCommentPattern = Pattern.compile("/blog/post/(\\d+)");


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String requestURI = req.getRequestURI();

        req.setAttribute("user", UserService.get((int)req.getSession().getAttribute("userId")));

        if (postListPattern.matcher(requestURI).matches()) {
            req.setAttribute("postList", PostService.extendedList());
            req.getRequestDispatcher("/post.jsp").forward(req, resp);
            return;
        }

        if (postCommentPattern.matcher(requestURI).matches()) {
            Matcher m = postCommentPattern.matcher(requestURI);
            m.find();
            int postId = Integer.parseInt(m.group(1));
            PostDts post = PostService.extendedGet(postId);
            post.setContent(post.getContent().replace("\n", "<br />"));
            req.setAttribute("post", post);
            List<CommentDts> commentList = CommentService.extendedListByPostId(postId);
            commentList.forEach(commentDts -> commentDts.setContent(commentDts.getContent().replace("\n", "<br />")));
            req.setAttribute("commentList", commentList);
            req.getRequestDispatcher("/comment.jsp").forward(req, resp);
            return;
        }

        resp.sendError(404);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        String topic = req.getParameter("topic");
        if (topic == "") topic = "[Без темы]";
        String content = req.getParameter("content");
        PostService.save(new Post(0, null, topic, content, (int)req.getSession().getAttribute("userId")));
        resp.sendRedirect("/blog/post");
    }
}