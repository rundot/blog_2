package ua.dp.rundot.controller;

import ua.dp.rundot.service.UserService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by rundot on 17.01.2017.
 */
public class SecurityFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)servletRequest;
        HttpSession session = req.getSession();
        int userId = 0;
        if (session.getAttribute("userId") != null)
            userId = (int)session.getAttribute("userId");
        if (userId > 0) {
            filterChain.doFilter(req, servletResponse);
        } else {
            HttpServletResponse resp = (HttpServletResponse)servletResponse;
            resp.sendRedirect("/blog/login");
        }
    }

    @Override
    public void destroy() {

    }
}
