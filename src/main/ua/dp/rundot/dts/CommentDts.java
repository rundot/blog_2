package ua.dp.rundot.dts;

import ua.dp.rundot.domain.Comment;
import ua.dp.rundot.domain.User;

/**
 * Created by rundot on 18.01.2017.
 */
public class CommentDts extends Comment {

    User user;

    public CommentDts(Comment comment) {
        super(comment.getId(), comment.getTimestamp(), comment.getContent(), comment.getUserId(), comment.getPostId());
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
