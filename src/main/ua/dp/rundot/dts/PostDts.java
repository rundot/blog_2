package ua.dp.rundot.dts;

import ua.dp.rundot.domain.Comment;
import ua.dp.rundot.domain.Post;
import ua.dp.rundot.domain.User;

import java.util.List;

/**
 * Created by rundot on 17.01.2017.
 */
public class PostDts extends Post {

    private List<Comment> commentList;
    private User user;

    public PostDts(Post post) {
        super(post.getId(), post.getTimestamp(), post.getTopic(), post.getContent(), post.getUserId());
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public User getUser() {
        return user;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }
}
