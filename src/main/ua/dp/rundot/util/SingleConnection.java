package ua.dp.rundot.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class SingleConnection {

    private static Connection connection;

    private SingleConnection() {}

    public static Connection getConnection() {
        if (connection == null) {
            try {
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection("jdbc:postgresql://rundot.dp.ua:5432/blog", "levelup", "levelup");
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
}