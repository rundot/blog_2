package ua.dp.rundot.dao;

import ua.dp.rundot.domain.Comment;

import java.util.List;

/**
 * Created by emaksimovich on 16.01.17.
 */
public interface CommentDao {
    int save(Comment comment);
    Comment get(int id);
    List<Comment> list();
}