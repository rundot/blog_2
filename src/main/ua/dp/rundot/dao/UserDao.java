package ua.dp.rundot.dao;

import ua.dp.rundot.domain.User;

import java.util.List;

/**
 * Created by emaksimovich on 16.01.17.
 */
public interface UserDao {
    int save(User user);
    User get(int id);
    List<User> list();

}