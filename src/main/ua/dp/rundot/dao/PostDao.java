package ua.dp.rundot.dao;

import ua.dp.rundot.domain.Post;

import java.util.List;

/**
 * Created by emaksimovich on 16.01.17.
 */
public interface PostDao {
    int save(Post post);
    Post get(int id);
    List<Post> list();
}