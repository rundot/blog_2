package ua.dp.rundot.dao.postgreSQL;

import ua.dp.rundot.dao.CommentDao;
import ua.dp.rundot.dao.PostDao;
import ua.dp.rundot.domain.Comment;
import ua.dp.rundot.domain.Post;
import ua.dp.rundot.service.UserService;
import ua.dp.rundot.util.Logger;
import ua.dp.rundot.util.SingleConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class CommentDaoImpl implements CommentDao {

    private Connection connection = SingleConnection.getConnection();

    @Override
    public int save(Comment comment) {
        if (connection == null) return 0;
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO comments (content, post_id, user_id) " +
                        "VALUES (?, ?, ?) RETURNING id;"
        )) {
            statement.setString(1, comment.getContent());
            statement.setInt(2, comment.getPostId());
            statement.setInt(3, comment.getUserId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int id = resultSet.getInt("id");
            resultSet.close();
            return id;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return 0;
    }

    @Override
    public Comment get(int id) {
        if (connection == null) return null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM comments WHERE id = ?;"
        )) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Comment comment = new Comment(
                    resultSet.getInt("id"),
                    resultSet.getTimestamp("timestamp"),
                    resultSet.getString("content"),
                    resultSet.getInt("user_id"),
                    resultSet.getInt("post_id")
            );
            resultSet.close();
            return comment;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return null;
    }

    @Override
    public List<Comment> list() {
        if (connection == null) return null;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM comments ORDER BY timestamp DESC;");
            List<Comment> commentList = new ArrayList<>();
            while (resultSet.next()) {
                commentList.add(new Comment(
                        resultSet.getInt("id"),
                        resultSet.getTimestamp("timestamp"),
                        resultSet.getString("content"),
                        resultSet.getInt("user_id"),
                        resultSet.getInt("post_id")
                ));
            }
            resultSet.close();
            return commentList;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {
        CommentDao commentDao = new CommentDaoImpl();
        int id;
        Comment comment = new Comment(0, null, "content", 1, 1);
        System.out.printf("Adding comment. Returning id: %d%n", id = commentDao.save(comment));
        System.out.printf("Getting comment with id %d: %s%n", id, commentDao.get(id));
        System.out.printf("Listing comments: %s%n", commentDao.list());
    }

}