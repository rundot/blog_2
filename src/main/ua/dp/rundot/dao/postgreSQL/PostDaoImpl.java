package ua.dp.rundot.dao.postgreSQL;

import ua.dp.rundot.dao.PostDao;
import ua.dp.rundot.dao.UserDao;
import ua.dp.rundot.domain.Post;
import ua.dp.rundot.domain.User;
import ua.dp.rundot.service.UserService;
import ua.dp.rundot.util.Logger;
import ua.dp.rundot.util.SingleConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class PostDaoImpl implements PostDao {

    private Connection connection = SingleConnection.getConnection();

    @Override
    public int save(Post post) {
        if (connection == null) return 0;
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO posts(topic, content, user_id) " +
                        "VALUES (?, ?, ?) RETURNING id;"
        )) {
            statement.setString(1, post.getTopic());
            statement.setString(2, post.getContent());
            statement.setInt(3, post.getUserId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int id = resultSet.getInt("id");
            resultSet.close();
            return id;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return 0;
    }

    @Override
    public Post get(int id) {
        if (connection == null) return null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM posts WHERE id = ?;"
        )) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Post post = new Post(
                    resultSet.getInt("id"),
                    resultSet.getTimestamp("timestamp"),
                    resultSet.getString("topic"),
                    resultSet.getString("content"),
                    resultSet.getInt("user_id")
            );
            resultSet.close();
            return post;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return null;
    }

    @Override
    public List<Post> list() {
        if (connection == null) return null;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM posts ORDER BY timestamp DESC;");
            List<Post> postList = new ArrayList<>();
            while (resultSet.next()) {
                postList.add(new Post(
                        resultSet.getInt("id"),
                        resultSet.getTimestamp("timestamp"),
                        resultSet.getString("topic"),
                        resultSet.getString("content"),
                        resultSet.getInt("user_id")
                ));
            }
            resultSet.close();
            return postList;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {
        PostDao postDao = new PostDaoImpl();
        int id;
        Post post = new Post(0, null, "topic", "content", 1);
        System.out.printf("Adding post. Returning id: %d%n", id = postDao.save(post));
        System.out.printf("Getting post with id %d: %s%n", id, postDao.get(id));
        System.out.printf("Listing posts: %s%n", postDao.list());
    }

}